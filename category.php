<?php
  global $marketeer_data;
	if(!is_front_page()){
		if(get_query_var('download_category')){
			get_template_part('category_home','downloads');
		}
		else if(isset($marketeer_data['grid_blog'])){
			switch($marketeer_data['grid_blog']){
			case 1:
				get_template_part('category_1','category');
			break;
			case 2:
				get_template_part('category_2','category');
			break;		
			case 3:
				get_template_part('category_3','category');
			break;	
			}
		}
		else {
			get_template_part('category_1','category');
		}
	} else {
		include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
		if(is_plugin_active( 'easy-digital-downloads/easy-digital-downloads.php')){	
			get_template_part('category_home','downloads');
		}
		else{
			get_template_part('category_1','category');
		}
	}

?>
<?php get_header(); 
global $marketeer_data;
wp_enqueue_script('marketeer_bxSlider');		?><!-- main content start --><div class="mainwrap blog <?php if(is_front_page()) echo 'home' ?> marketeer-grid">	<div class="main clearfix">		<div class="pad"></div>					<div class="content blog">					<?php 
			
			$count = $advertise = 0;
			if (have_posts()) : ?>						<?php while (have_posts()) : the_post(); ?>			<?php if(is_sticky(get_the_id())) { ?>			<div class="marketeer_sticky">			<?php } ?>			<?php
			$count++;$advertise++;			$postmeta = get_post_custom(get_the_id()); 
			if($advertise == 4){ ?>
			<?php if(isset($postmeta["subtitle"][0])) { ?>
				<div class="subtitle">
					<?php marketeer_security($postmeta["subtitle"][0]); ?>
				</div>				
			<?php } ?>				
			<?php } ?>				
		
			<?php			if ( has_post_format( 'gallery' , get_the_id())) { 			?>			<div class="slider-category <?php if($count == 3){ echo 'last';$count=0;}?>">								<div class="blogpostcategory">
				<?php get_template_part('includes/boxes/topBlogGrid','single'); ?>						<?php
					$attachments = '';
					add_filter( 'shortcode_atts_gallery', 'marketeer_gallery' );
					function marketeer_gallery( $out )
					{
						remove_filter( current_filter(), __FUNCTION__ );
						$out['size'] = 'marketeer-news';
						return $out;
					}
					$attachments =  get_post_gallery_images( $post->ID);
					if ($attachments) {?>
						<div id="slider-category" class="slider-category">
						<script type="text/javascript">
						jQuery(document).ready(function($){
							jQuery('.bxslider').bxSlider({
							  easing : 'easeInOutQuint',
							  captions: true,
							  speed: 800,
							   buildPager: function(slideIndex){
								switch(slideIndex){
								<?php
								$i = 0;
								foreach ($attachments as  $image_url) { 
									echo 'case '.$i.':return "<img src=\"'. esc_url( $image_url) .'\"";';
									$i++;
								} ?>									
								}
							  }
							});
						});	
						</script>	
							<ul id="slider" class="bxslider">
								<?php 
									foreach ($attachments as  $image_url) {
									

										?>	
											<li>
												<img src="<?php echo esc_url( $image_url) ?>" alt="<?php ?>"/>							
											</li>
											<?php } ?>
							</ul>

						</div>
					<?php } ?>
				<?php
					$attachments = '';
					add_filter( 'shortcode_atts_gallery', 'marketeer_gallery' );
					function marketeer_gallery( $out )
					{
						remove_filter( current_filter(), __FUNCTION__ );
						$out['size'] = 'marketeer-news';
						return $out;
					}
					$attachments =  get_post_gallery_images( $post->ID);
					if ($attachments) {?>
						<div id="slider-category" class="slider-category">
						<script type="text/javascript">
						jQuery(document).ready(function($){
							jQuery('.bxslider').bxSlider({
							  easing : 'easeInOutQuint',
							  captions: true,
							  speed: 800,
							   buildPager: function(slideIndex){
								switch(slideIndex){
								<?php
								$i = 0;
								foreach ($attachments as  $image_url) { 
									echo 'case '.$i.':return "<img src=\"'. esc_url( $image_url) .'\"";';
									$i++;
								} ?>									
								}
							  }
							});
						});	
						</script>	
							<ul id="slider" class="bxslider">
								<?php 
									foreach ($attachments as  $image_url) {
									

										?>	
											<li>
												<img src="<?php echo esc_url( $image_url) ?>" alt="<?php ?>"/>							
											</li>
											<?php } ?>
							</ul>

						</div>
					<?php } ?>
				<?php if($count == 2) { ?>						<?php // get_template_part('includes/boxes/loopBlogGrid','single'); ?>
				<?php } ?>
				<?php if(isset($postmeta["subtitle"][0])) { ?>
					<div class="subtitle">
						<?php marketeer_security($postmeta["subtitle"][0]); ?>
					</div>				
				<?php } ?>						</div>			</div>			<?php } 			if ( has_post_format( 'video' , get_the_id())) { ?>			<div class="slider-category <?php if($count == 3){ echo 'last';$count=0;}?>">							<div class="blogpostcategory">
					<?php get_template_part('includes/boxes/topBlogGrid','single'); ?>									<?php										if(!empty($postmeta["video_post_url"][0])) {
						echo do_shortcode('[video src='.esc_url($postmeta["video_post_url"][0]).' width=800 height=490]');
					}					else{ 						  $image = 'http://placehold.it/800x490'; 						  					?>						  <a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php echo marketeer_getImage(get_the_id(),'marketeer-postGridBlock'); ?></a>											<?php } ?>	
					<?php if($count == 2) { ?>					<?php // get_template_part('includes/boxes/loopBlogGrid','single'); ?>
					<?php } ?>
				<?php if(isset($postmeta["subtitle"][0])) { ?>
					<div class="subtitle">
						<?php marketeer_security($postmeta["subtitle"][0]); ?>
					</div>				
				<?php } ?>			
				</div>
							</div>			<?php } 			if ( has_post_format( 'link' , get_the_id())) {
			$count--;
			?>			<?php 			} 	
			if ( has_post_format( 'quote' , get_the_id())) {?>
			<div class="quote-category <?php if($count == 3){ echo 'last';$count=0;}?>">
				<div class="blogpostcategory quote-category">				
					<?php get_template_part('includes/boxes/loopBlogQuoteGrid','single'); ?>								
				</div>
			</div>
			
			<?php 
			} 						?>			<?php if ( has_post_format( 'audio' , get_the_id())) {?>			<div class="blogpostcategory <?php if($count == 3){ echo 'last';$count=0;}?>">
				<?php get_template_part('includes/boxes/topBlogGrid','single'); ?>
					<div class="audioPlayerWrap">						<div class="audioPlayer">
							<?php 
							if(isset($postmeta["audio_post_url"][0]))
								echo do_shortcode('[soundcloud  params="auto_play=false&hide_related=false&visual=true" width="100%" height="150"]'. esc_url($postmeta["audio_post_url"][0]) .'[/soundcloud]') ?>
						</div>					</div>
				<?php if($count == 2) { ?>					<?php // get_template_part('includes/boxes/loopBlogGrid','single'); ?>	
				<?php } ?>	
			<div class="subtitle">
				<?php marketeer_security($postmeta["subtitle"][0]); ?>
			</div>							</div>				<?php			}			?>														<?php			if ( !get_post_format() ) {?>		
			<div class="blogpostcategory <?php if($count == 3){ echo 'last';$count=0;}?>">				<?php get_template_part('includes/boxes/topBlogGrid','single'); ?>								<?php if(marketeer_getImage(get_the_id(), 'marketeer-postGridBlock') != '' && (!isset($postmeta["marketeer_featured_category"][0]) || $postmeta["marketeer_featured_category"][0] == 1)) { ?>	
					<a class="overdefultlink" href="<?php the_permalink() ?>">					<div class="overdefult">					</div>					</a>
					<div class="blogimage">							<div class="loading"></div>								<a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php echo marketeer_getImage(get_the_id(), 'marketeer-postGridBlock'); ?></a>					</div>					<?php } ?>
					<?php if(!has_post_thumbnail(get_the_id())) { ?>					<?php  get_template_part('includes/boxes/loopBlogGrid','single'); ?>
					<?php } ?>
			<?php if(isset($postmeta["subtitle"][0])) { ?>
				<div class="subtitle">
					<?php marketeer_security($postmeta["subtitle"][0]); ?>
				</div>				
			<?php } ?>			</div>
						<?php } ?>					<?php if(is_sticky()) { ?>				</div>			<?php } ?>
							<?php 
				endwhile; ?>									<?php											get_template_part('includes/wp-pagenavi','navigation');						if(function_exists('wp_pagenavi')) { wp_pagenavi(); }					?>										<?php else : ?>											<div class="postcontent">							<h1><?php marketeer_security($marketeer_data['errorpagetitle']) ?></h1>							<div class="posttext">								<?php marketeer_security($marketeer_data['errorpage']) ?>							</div>						</div>											<?php endif; ?>						</div>
	</div>	</div>											<?php get_footer(); ?>
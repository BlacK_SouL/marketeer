<?php
global $marketeer_data; 
$use_bg = ''; $background = ''; $custom_bg = ''; $body_face = ''; $use_bg_full = ''; $bg_img = ''; $bg_prop = '';



if(isset($marketeer_data['background_image_full'])) {
	$use_bg_full = $marketeer_data['background_image_full'];
	
}

if(isset($marketeer_data['use_boxed'])){
	$use_boxed = $marketeer_data['use_boxed'];
}
else{
	$use_boxed = 0;
}

if($use_bg_full) {


	if($use_bg_full && isset($marketeer_data['use_boxed']) && $marketeer_data['use_boxed'] == 1) {
		$bg_img = $marketeer_data['image_background'];
		$bg_prop = '';
	}

	

	
	$background = 'url('. $bg_img .') '.$bg_prop ;

}

function ieOpacity($opacityIn){
	
	$opacity = explode('.',$opacityIn);
	if($opacity[0] == 1)
		$opacity = 100;
	else
		$opacity = $opacity[1] * 10;
		
	return $opacity;
}

function HexToRGB($hex,$opacity) {
		$hex = preg_replace("/#/", "", $hex);
		$color = array();
 
		if(strlen($hex) == 3) {
			$color['r'] = hexdec(substr($hex, 0, 1) . $r);
			$color['g'] = hexdec(substr($hex, 1, 1) . $g);
			$color['b'] = hexdec(substr($hex, 2, 1) . $b);
		}
		else if(strlen($hex) == 6) {
			$color['r'] = hexdec(substr($hex, 0, 2));
			$color['g'] = hexdec(substr($hex, 2, 2));
			$color['b'] = hexdec(substr($hex, 4, 2));
		}
 
		return 'rgba('.$color['r'] .','.$color['g'].','.$color['b'].','.$opacity.')';
	}
	
	if(isset($marketeer_data['google_menu_custom']) && $marketeer_data['google_menu_custom'] != ''){
		$font_menu = explode(':',$marketeer_data['google_menu_custom']);
		if(count($font_menu)>1) {
			$font_menu = $font_menu[0];
		}
		else{
			$font_menu = $marketeer_data['google_menu_custom'];
		}
	}else{
		$font_menu = explode(':',$font_menu);
		if(count($font_menu)>1) {
			$font_menu = $font_menu[0];
		}
		else{
			$font_menu = $font_menu;
		}
	}		
	
	if(isset($marketeer_data['google_quote_custom']) && $marketeer_data['google_quote_custom'] != ''){
		$font_quote = explode(':',$marketeer_data['google_quote_custom']);
		if(count($font_quote)>1) {
			$font_quote = $font_quote[0];
		}
		else{
			$font_quote = $marketeer_data['google_quote_custom'];
		}
	}else{
		$font_quote = explode(':',$font_quote);
		if(count($font_quote)>1) {
			$font_quote = $font_quote[0];
		}
		else{
			$font_quote = $font_quote;
		}
	}	

	if(isset($marketeer_data['google_heading_custom']) && $marketeer_data['google_heading_custom'] != ''){
		$font_heading = explode(':',$marketeer_data['google_heading_custom']);
		if(count($font_heading)>1) {
			$font_heading = $font_heading[0];
		}
		else{
			$font_heading= $marketeer_data['google_heading_custom'];
		}	
	}else{
		$font_heading = explode(':',$font_heading);
		if(count($font_heading)>1) {
			$font_heading = $font_heading[0];
		}
		else{
			$font_heading=$font_heading;
		}	
	}

	if(isset($marketeer_data['google_body_custom']) && $marketeer_data['google_body_custom'] != ''){
		$font_body = explode(':',$marketeer_data['google_body_custom']);
		if(count($font_body)>1) {
			$font_body = $font_body[0];
		}
		else{
			$font_body = $marketeer_data['google_body_custom'];
		}
	}else{
		$font_body = explode(':',$font_body);
		if(count($font_body)>1) {
			$font_body = $font_body[0];
		}
		else{
			$font_body = $font_body;
		}		
	}	

?>


.block_footer_text, .quote-category .blogpostcategory {font-family: <?php echo $font_quote; ?>, "Helvetica Neue", Arial, Helvetica, Verdana, sans-serif;}
body {	 
	background:<?php echo $marketeer_data['body_background_color'].' '.$background ?>  !important;
	color:<?php echo $marketeer_data['body_font']['color']; ?>;
	font-family: <?php echo $font_body; ?>, "Helvetica Neue", Arial, Helvetica, Verdana, sans-serif;
	font-size: <?php echo $marketeer_data['body_font']['size']; ?>;
	font-weight: normal;
}

::selection { background: #000; color:#fff; text-shadow: none; }

h1, h2, h3, h4, h5, h6, .block1 p {font-family: <?php echo $font_heading; ?>, "Helvetica Neue", Arial, Helvetica, Verdana, sans-serif;}
h1 { 	
	color:<?php echo $marketeer_data['heading_font_h1']['color']; ?>;
	font-size: <?php echo $marketeer_data['heading_font_h1']['size'] ?> !important;
	}
	
h2, .term-description p { 	
	color:<?php echo $marketeer_data['heading_font_h2']['color']; ?>;
	font-size: <?php echo $marketeer_data['heading_font_h2']['size'] ?> !important;
	}

h3 { 	
	color:<?php echo $marketeer_data['heading_font_h3']['color']; ?>;
	font-size: <?php echo $marketeer_data['heading_font_h3']['size'] ?> !important;
	}

h4 { 	
	color:<?php echo $marketeer_data['heading_font_h4']['color']; ?>;
	font-size: <?php echo $marketeer_data['heading_font_h4']['size'] ?> !important;
	}	
	
h5 { 	
	color:<?php echo $marketeer_data['heading_font_h5']['color']; ?>;
	font-size: <?php echo $marketeer_data['heading_font_h5']['size'] ?> !important;
	}	

h6 { 	
	color:<?php echo $marketeer_data['heading_font_h6']['color']; ?>;
	font-size: <?php echo $marketeer_data['heading_font_h6']['size'] ?> !important;
	}	

.pagenav a {font-family: <?php echo $font_menu; ?> !important;
			  font-size: <?php echo $marketeer_data['menu_font']['size'] ?>;
			  font-weight:<?php echo $marketeer_data['menu_font']['style'] ?>;
			  color:<?php echo $marketeer_data['menu_font']['color'] ?>;
}
.block1_lower_text p,.widget_wysija_cont .updated, .widget_wysija_cont .login .message, p.edd-logged-in, #edd_login_form, #edd_login_form p  {font-family: <?php echo $font_body; ?>, "Helvetica Neue", Arial, Helvetica, Verdana, sans-serif !important;color:#444;font-size:14px;}

a, select, input, textarea, button{ color:<?php echo $marketeer_data['body_link_coler']; ?>;}
h3#reply-title, select, input, textarea, button, .link-category .title a{font-family: <?php echo $font_body; ?>, "Helvetica Neue", Arial, Helvetica, Verdana, sans-serif;}

.prev-post-title, .next-post-title, .blogmore, .more-link {font-family: <?php echo $font_heading; ?>, "Helvetica Neue", Arial, Helvetica, Verdana, sans-serif;}

/* ***********************
--------------------------------------
------------MAIN COLOR----------
--------------------------------------
*********************** */

a:hover, span, .current-menu-item a, .blogmore, .more-link, .pagenav.fixedmenu li a:hover, .widget ul li a:hover,.pagenav.fixedmenu li.current-menu-item > a,.block2_text a,
.blogcontent a, .sentry a, .post-meta a:hover, .sidebar .social_icons i:hover, .content.blog .single-date

{
	color:<?php echo $marketeer_data['mainColor']; ?>;
}

.su-quote-style-default  {border-left:5px solid <?php echo $marketeer_data['mainColor']; ?>;}

 
/* ***********************
--------------------------------------
------------BACKGROUND MAIN COLOR----------
--------------------------------------
*********************** */

.top-cart, .blog_social .addthis_toolbox a:hover, .widget_tag_cloud a:hover, .sidebar .widget_search #searchsubmit,
.menu ul.sub-menu li:hover, .specificComment .comment-reply-link:hover, #submit:hover, .addthis_toolbox a:hover, .wpcf7-submit:hover, #submit:hover,
.link-title-previous:hover, .link-title-next:hover, .specificComment .comment-edit-link:hover, .specificComment .comment-reply-link:hover, h3#reply-title small a:hover, .pagenav li a:after,
.widget_wysija_cont .wysija-submit,.sidebar-buy-button a, .widget ul li:before, #footer .widget_search #searchsubmit, .marketeer-read-more a:hover, .blogpost .tags a:hover,
.mainwrap.single-default.sidebar .link-title-next:hover, .mainwrap.single-default.sidebar .link-title-previous:hover, .marketeer-home-deals-more a:hover, .top-search-form i:hover, .edd-submit.button.blue:hover,
ul#menu-top-menu, a.catlink:hover
  {
	background:<?php echo $marketeer_data['mainColor']; ?> ;
}
.pagenav  li li a:hover {background:none;}
.edd-submit.button.blue:hover, .cart_item.edd_checkout a:hover {background:<?php echo $marketeer_data['mainColor']; ?> !important;}
.link-title-previous:hover, .link-title-next:hover {color:#fff;}
#headerwrap {background:<?php echo $marketeer_data['menu_background_color']; ?>;border-top:5px solid <?php echo $marketeer_data['mainColor']; ?>;}


 /* ***********************
--------------------------------------
------------BOXED---------------------
-----------------------------------*/
<?php if($use_boxed == 0 &&  isset($marketeer_data['use_background']) && $marketeer_data['use_background'] == 1){ ?>
	body, .cf, .mainwrap, .post-full-width, .titleborderh2, .sidebar  {
	background:<?php echo $marketeer_data['body_background_color'].' '.$background ?>  !important; 
	}
	
<?php	} ?>
 <?php if(isset($marketeer_data['use_boxed']) &&  $use_boxed == 1){ ?>
header,.outerpagewrap{background:none !important;}
header,.outerpagewrap,.mainwrap{background-color:<?php echo $marketeer_data['body_background_color'] ?> ;}
@media screen and (min-width:1220px){
body {width:1220px !important;margin:0 auto !important;}
.top-nav ul{margin-right: -21px !important;}
.mainwrap.shop {float:none;}
.pagenav.fixedmenu { width: 1220px !important;}
.bottom-support-tab,.totop{right:5px;}
<?php if($use_bg_full){ ?>
	body {
	background:<?php echo $marketeer_data['body_background_color'].' '.$background ?>  !important; 
	background-attachment:fixed !important;
	background-size:cover !important; 
	-webkit-box-shadow: 0px 0px 5px 1px rgba(0,0,0,0.2);
-moz-box-shadow: 0px 0px 5px 1px rgba(0,0,0,0.2);
box-shadow: 0px 0px 5px 1px rgba(0,0,0,0.2);
	}	
<?php	} ?>
 <?php if(!$use_bg_full){ ?>
	body {
	background:<?php echo $marketeer_data['body_background_color'].' '.$background ?>  !important; 
	
	}
	
<?php	} ?>	
}
<?php } ?>
 
 
 
 
/* ***********************
--------------------------------------
------------RESPONSIVE MODE----------
--------------------------------------
*********************** */

<?php if(isset($marketeer_data['showresponsive']) && $marketeer_data['showresponsive'] == 1 ) { ?>

@media screen and (min-width:0px) and (max-width:1220px){
	
	/* MENU */
	
	.pagenav {width:100% !important;padding: 0px 3.2%;margin-left:-3.2%;width:100%;float:left;background:#fff;border-top: 1px solid #eee !important;}
	.pagenav .social_icons {position:relative;text-align:center ;left:50%;margin:0 auto !important;margin-left:-90px !important;float: none;margin-top: 14px !important;}
	#headerwrap {margin-bottom:50px;}
	.menu > li {padding-bottom:0;}
    #menu-main-menu-container{width:100% !important;float:left;text-align:center;border-bottom:1px solid #eee }
	.pagenav li a {padding:15px 15px 7px 15px;}
	#logo {width:100%;margin:40px 0 0 0px !important;float:left;}
	.top-search-form {margin:-15px 10px 15px 0px;}
	ul#menu-top-menu {top:-1px;right:50%;padding-left:0;margin-right:-80px;}
	ul#menu-top-menu li:first-child a {border:none;}
	
	.news-wrapper-content{margin-top:80px;}
	.top-search-form input {top:84px;}
	
	.mainwrap {padding-top:50px;}
	.page .mainwrap{padding-top:0px;}
	.menu > li.has-sub-menu:after{margin-top:14px;}
	.relatedPosts {min-width:initial;width:auto !important;}
	.main, #footerb, #footer, .news-wrapper-content {width: 94%; padding-left:3%;padding-right:3%; }
	.pagenav .menu {padding-left:3%;}
	.news-wrapper-content .news {width:auto;}
	.top-nav {width: 100%; padding-left:0%;padding-right:0%;}
	.pagecontent, .block2_content, #footerinside{width:100%;}
    .blogimage img, .blogsingleimage img, .related img, #slider-category img{width:100%;height:auto;}
	.bx-viewport {height:auto;}
	.pagenav .social_icons {float:left;}
	
	.block2_social:before, .social_text, .pagenav.fixedmenu {display:none !important;}
	.block2_social .social_content  {margin-left:0;}
	.block2_social .social_content {margin-top:0;}
	.block2_social {top:0; padding:10px;background:#fff;}
	.block1 p, .block1 a:hover p {font-size:16px;}
	
	/* BLOG */
	
	.widget h3:after {margin-left:47.5%;}
	.blog_social, .socialsingle {background-position: -11px 21px;}
	.right-part {width:85%;}
	.mainwrap.single-default.sidebar .right-part {width:70% !important;}
	.mainwrap.single-default.sidebar .related img{max-width:initial;}
	
	.sidebar.default .blogpostcategory {width:91%;}
	.singledefult .blogpost {min-width:initial;}
	
	/* GRID BLOG */
	
	#marketeer-grid-sort {width:100%;}
	.marketeer-grid .blogpostcategory, .marketeer-grid .content.blog {width:initial;}
	.marketeer-grid .blogpostcategory {height:auto !important; }
	
	.marketeer-grid-2 .marketeer-blog-image{width:100%;}
	.marketeer-grid-2 .marketeer-blog-image img {max-width:100% !important;}
	.marketeer-grid-2 .marketeer-blog-content {width:100%;border-bottom:5px solid #eee;min-height:250px;}
	.marketeer-grid-2 .marketeer-blog-content.left {margin-left:0;}
	.marketeer-grid-2 .marketeer-blog-image.right {margin-right:0;}
	.marketeer-grid-2 .marketeer-blog-content.left .blog-category, .marketeer-grid-2 .marketeer-blog-content.left h2.title , .marketeer-grid-2 .marketeer-blog-content.left .post-meta, .marketeer-grid-2 .marketeer-blog-content.left p  {text-align:left !important;}
	.marketeer-grid-2.sidebar .blogpostcategory {min-width:100%;}
	.sidebar.default .blogpostcategory {min-width:initial;}
	
	/* FOOTER */
	.lowerfooter {height:2px;padding:0;margin-top:0px;}
	.footer_widget3 {float:left;}
	
	.left-footer-content, .right-footer-content {margin-top:30px;}
	
	/* WITH SIDEBAR */
	
	.mainwrap.sidebar .content.blog, .mainwrap.single-default.sidebar .main .content.singledefult,.sidebar .content.singlepage{width:60% !important;margin-right:1% !important;}
	.sidebar .content.singlepage {width:55% !important;}
	.mainwrap.sidebar .postcontent, .mainwrap.single-default.sidebar .content.singledefult .related img  {width:100% !important;}
	.mainwrap .sidebar {width: 27.4% !important; float: left; }	
	.widget-line {width:100%;}
	.mainwrap.blog.sidebar .main .content.blog .blogimage img, .mainwrap.single-default.sidebar .main .content.singledefult .blogsingleimage img {padding:0;}
	.link-category .title, .sidebar .link-category .title {display:block;float:left;position:relative;width:100%;margin-top:0;padding:0 !important; }
	.su-column img {height:auto;}
	
	.block2_text {width:48%;max-height:initial;}
	
	.sidebar-deals {width:100%;}
	.page .widget.deals {width:90% !important;}
}


@media screen and (min-width:0px) and (max-width:960px){
	
	
	
	textarea#comment {width:85%;}
	.pagenav .menu, .postcontent.singledefult .share-post::before, .postcontent.singledefult .share-post::after, .blog-category:before, .blog-category:after{display:none;}
	.pagenav {padding: 0px 3.2%;margin-left:-3.2%;width:100%;float:left;border-top: 1px solid #eee;}
	.pagenav .social_icons {position:relative;text-align:center ;left:50%;margin:0 auto !important;margin-left:-95px !important;float: none;margin-top: 14px !important;}
	#headerwrap {margin-bottom:50px;}
	
	/* MENU */
	
	.respMenu {width:100% !important;float:none !important; text-transform:uppercase;background:#fff;background:rgba(255,255,255,1);border-left:1px solid #eee;border-right:1px solid #eee; text-align: center; color:#121212;font-weight:normal;     cursor:pointer;display:block;}
	.resp_menu_button {font-size:14px;position:absolute;display:inline-block; text-align:center;margin:0 auto;top:16px;color:#222;z-index:9999;width:32px;height:24px;margin-left:-16px;}
	.respMenu.noscroll a i {display:none;}
	
	.respMenu .menu-main-menu-container {margin-top:60px;}
	.event-type-selector-dropdown {display:none;margin-top:60px;}
	.respMenu a{border-left:1px solid #eee;border-right:1px solid #eee; background:#fff;width:94%;font-size:14px;font-weight:bold;padding:10px 3%; border-bottom:1px solid #ddd;text-transform:uppercase !important;float:left;text-align: left !important;text-transform:none;font-weight:normal;}
	
	.top-search-form {margin:-30px 10px 25px 0px;}
	#logo img {margin-bottom:10px;}
	
	.right-part {width:80%;}
	.mainwrap.single-default.sidebar .right-part {width:55% !important;}
	.blog_social, .socialsingle {margin-top:45px;}
	textarea {width:97%;}
	
	.mainwrap.blog .blog_social,.mainwrap.single-default .blog_social {margin:0 0 30px 0;}
	.mainwrap.single-default .blog_social {margin-left:30px;}
	.quote-category .blogpostcategory .meta p:before, .quote-category .blogpostcategory .meta p:after {display:none;}
	.quote-category .blogpostcategory p {text-indent:0;}
	
	.block2_text{margin-left:0; background:none;-webkit-box-shadow:none;-moz-box-shadow:none;box-shadow:none;width:90%;}
	.block2_img {text-align:center;background:none;width:100%;padding:35px 0;}
	.block2_img .block2_img_big{float:none}
	.block2 {background:#fff;}
	
	/* SINGLE PRODUCT */
	
	.single-download .mainwrap.sidebar .sidebar.product-page, .single-download .mainwrap.sidebar .sidebar {min-width:30%;}
	
}

@media screen and (min-width:0px) and (max-width:768px){
	h1 {font-size:48px !important;}
	h2 {font-size:36px !important;}
	h3 {font-size:28px !important;}
	h4 {font-size:24px !important;}
	h5 {font-size:22px !important;}
	h6 {font-size:18px !important;}
    .right-part{width:78%;}
	.mainwrap.single-default.sidebar .right-part  {width:75% !important;}

	.link-title-next {float:left;padding-left:25px;}
	.link-title-next span, .next-post-title {float:left;text-align:left;}
	
	.blog-category em:before,.blog-category em:after{display:none;}
	
	/* WITH SIDEBAR */
	.bibliographical-info {padding:0 20px;}
	.mainwrap .sidebar {width:94% !important;float:left !important;margin-left:0;margin-top:40px !important;}
	.mainwrap.blog.sidebar .sidebar, .mainwrap.single-default.sidebar .sidebar, .mainwrap.sidebar .sidebar, .sidebar .widget {margin-left:0;}
	.mainwrap.sidebar .content.blog, .mainwrap.single-default.sidebar .main .content.singledefult,.sidebar .content.singlepage {width:100% !important;}
	.sidebar .content.singlepage, .content.singlepage {width:85% !important;}
	#footer .wttitle {float:none;}
	
	.marketeer-grid .content.blog .blogpostcategory, .marketeer-grid .content.blog .blogpostcategory, .marketeer-grid .content.blog .slider-category{margin-right:0;}
	a.catlink {margin-bottom:20px;}
}

@media screen and (min-width:0px) and (max-width:720px){
	#footer .widget h3{text-align:center;}
	.footer_widget1, .footer_widget2, .footer_widget3 {width:100%;text-align:center;}
	.footer_widget3 {margin-bottom:30px;}
	.footer_widget2 .widget.widget_text p {padding-left:0;}
	#footer .social_icons {float:left;margin-top:20px;position:relative;left:50%;margin-left:-100px;  }
	.footer_widget1 {margin-bottom:30px;}
	
	#footerb .copyright {float:left;margin-top:30px; text-align:center;}
	.right-part {width:75%;}
	.widget-date {text-align:left;}

	img.alignleft, img.alignright {width:100%;height:auto;margin-bottom:20px;}
}

@media screen and (min-width:0px) and (max-width:620px){
	.marketeer-grid-2 .marketeer-blog-content {padding-bottom:25px;}
	.quote-category .blogpostcategory {font-size:24px;line-height:34px;}
	.mainwrap.blog.sidebar h2.title, .mainwrap.single-default.sidebar h2.title {font-size:24px !important;}
	.block1 a{width:94%;padding-left:3%;padding-right:3%; float:left;}
	.block1 p, .block1 a:hover p {}
	.block1 {background:#FAFAFA;}
	.block2_social {width: 100%;left:0;margin:0; position:relative;float:left;background:#f4f4f4 !important;padding:25px 0 15px 0;}
	.block2_social .social_content {width:auto;}
	
	.left-footer-content, .right-footer-content {width:100%;float:left;text-align:center;margin-top:0;}
	
	.related .one_third {width:100%;margin-bottom:30px;}
	.right-part {width:70%;}
	
	.mainwrap.blog .blog_social,.mainwrap.single-default .blog_social {float:left !important;margin-top:30px !important;}
	.addthis_toolbox a:first-child{margin-left:0;}
	
	.post-meta {margin-left:0;}
	.post-meta a:after {display:none;}
	.blog_social, .socialsingle {float:left;margin:10px 0;}
	.post-meta a {width:100%;float:left;}
	.bottomBlog {float:left;}
	
	.block2_content {margin-top:10px;}
	
	/* INSTAGRAM */
	
	h5.block3-instagram-title {font-size:36px !important;}
	.block3-instagram-username {display:none;}
	.link-category .title a {line-height:40px;}
	
	/* NEWSLETTER */
	
	.news-wrapper-content .news .widget_wysija_cont form {float:left;width:100%;}
	.news-wrapper-content .news .widget_wysija_cont .wysija-submit, .news-wrapper-content .news .widget_wysija input {width:100%;text-align:center;margin:0 !important;float:left;padding:0 !important;}
	.news-wrapper-content .news .widget_wysija_cont .wysija-submit {margin-bottom:20px !important;}
	.news-wrapper-content .news .wysija-paragraph {float:none;}
	
	.single .post-meta em {display:none;}
}
	
	
@media screen and (min-width:0px) and (max-width:515px){	
	.specificComment .blogAuthor {display:none;}
	.right-part {width:100%;}
	.mainwrap.single-default.sidebar .right-part  {width:100% !important;}
	h1 {font-size:40px !important;}
	h2, .mainwrap.blog.sidebar h2.title, .mainwrap.single-default.sidebar h2.title {font-size:20px !important;}
	h3 {font-size:24px !important;}
	h4 {font-size:20px !important;}
	h5 {font-size:18px !important;}
	h6 {font-size:16px !important;}
	.blog_category {font-size:13px;}
	.gallery-single {text-align:center;}
	.image-gallery, .gallery-item {float:none;}
	
	.post-meta:after {display:none;}
	.post-meta{padding:0 15px 0 0px !important;font-size:12px !important;}
	
	
	.block2_text{width:80%;}
}

@media screen\0 {
	 .resp_menu_button{margin-left:48%;}
}

@media screen and (min-width:0px) and (max-width:415px){	
	
}

@media 
(-webkit-min-device-pixel-ratio: 2), 
(min-resolution: 192dpi) { 
 
	}
	
	
<?php } ?>

/* ***********************
--------------------------------------
------------CUSTOM CSS----------
--------------------------------------
*********************** */

<?php echo marketeer_stripText($marketeer_data['custom_style']) ?>
<?php get_header(); 
global $marketeer_data;
	?><!-- main content start --><div class="mainwrap blog <?php if(is_front_page()) echo 'home' ?> <?php if(!isset($marketeer_data['use_fullwidth'])) echo 'sidebar' ?> marketeer-grid-2">	<div class="main clearfix">		<div class="pad"></div>					<div class="content blog">					<?php 
			
			$count = 'even';
			if (have_posts()) : ?>						<?php while (have_posts()) : the_post(); ?>			<?php if(is_sticky(get_the_id())) { ?>			<div class="marketeer_sticky">			<?php } ?>			<?php			$postmeta = get_post_custom(get_the_id()); ?>
			
			<div class="slider-category blogpostcategory">
			<?php
			if($count == 'even' || $count == "even-temp"  ){ ?>	
				<?php if($count == "even-temp"){ ?>
					<div class="marketeer-blog-content left <?php if ( !has_post_thumbnail(get_the_id()) ) echo 'full'; ?>">
						<?php get_template_part('includes/boxes/topBlog','single'); ?>
						<?php get_template_part('includes/boxes/loopBlogGrid','single'); ?>	
					</div>	
				<?php } ?>
				<?php if ($count == "even") $count="odd"; ?>
				<?php if ( has_post_thumbnail(get_the_id()) ) { ?> 
					<div class="marketeer-blog-image <?php if($count == "even-temp") echo 'right'; ?>">
						<?php get_template_part('includes/boxes/contentBlog','single'); ?>	
					</div>
				<?php } ?>
				<?php if($count == "even-temp") { 
					$count = "even"; 
					echo '</div>';
					continue;
				} ?>
			<?php } ?>
				
			<?php if($count=="odd") { ?>
				<?php $count = "even-temp"; ?>
				<div class="marketeer-blog-content <?php if ( !has_post_thumbnail(get_the_id()) ) echo 'full'; ?>">
					<?php if(!has_post_format( 'link' , get_the_id()) && !has_post_format( 'qoute' , get_the_id())) { ?>
						<?php get_template_part('includes/boxes/topBlog','single'); ?>
					<?php } ?>
					<?php if(has_post_format( 'link' , get_the_id())) { ?>
						<?php get_template_part('includes/boxes/loopBlogLinkGrid','single'); ?>
					<?php } else if(has_post_format( 'qoute' , get_the_id())) { ?>
						<?php get_template_part('includes/boxes/loopBlogQuoteGrid','single'); ?>
					<?php } else { ?>
						<?php get_template_part('includes/boxes/loopBlogGrid','single'); ?>	
					<?php } ?>
				</div>
			<?php } ?>			</div>				<?php if(is_sticky()) { ?>				</div>			<?php } ?>
							<?php 
				endwhile; ?>									<?php											get_template_part('includes/wp-pagenavi','navigation');						if(function_exists('wp_pagenavi')) { wp_pagenavi(); }					?>										<?php else : ?>											<div class="postcontent">							<h1><?php marketeer_security($marketeer_data['errorpagetitle']) ?></h1>							<div class="posttext">								<?php marketeer_security($marketeer_data['errorpage']) ?>							</div>						</div>											<?php endif; ?>						</div>		<!-- sidebar -->			<div class="sidebar">					<?php dynamic_sidebar( 'sidebar' ); ?>			</div>	</div>	</div>											<?php get_footer(); ?>